import yaml

with open("contacts.yaml",'r') as flux:
	print(yaml.load(flux))


class Person:
	def __init__(self, prenom=None, nom=None, adresse=None):
		self.prenom = prenom
		self.nom = nom
		self.adresse = adresse

	@staticmethod
	def from_file(nomfic):
		res = []
		with open(nomfic,'r') as flux:
			personnes = yaml.load(flux)
		for p in personnes:
			res.append(Person(p["prenom"], p["nom"], p["adresse"]))
		return res

	def __repr__(self ):
		return "<Person %s,%s, %s>" %(self.prenom, self.nom, self.adresse)



lp = Person.from_file('contacts.yaml')
for p in lp:
	print(p)
