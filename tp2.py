class Box:
    def __init__(self):
        self._contents = []
        self._ouvert = False
        self._capacity = None

    def add(self,truc):
        self._contents.append(truc)

    def action_add(self, truc):
        if self._ouvert and self.has_room_for(truc):
            self.add(truc)
            if self.capacity() is not None:
                self._capacity -= truc.volume()
            return True
        else:
            return False

    def __contains__(self,machin):
        return machin in self._contents

    def remove(self,machin):
        self._contents.remove(machin)
        if self._capacity is not None:
            self._capacity += machin.volume()

    def action_look(self):
        if not self.is_open():
            return "la boite est fermée !"
        else:
            return "la boite contient: "+', '.join(self._contents)

    def is_open(self):
        return self._ouvert

    def open(self):
        self._ouvert = True

    def close(self):
        self._ouvert = False

    def has_room_for(self,truc):
        return self._capacity is None or self._capacity >= truc.volume()

    def set_capacity(self, capacity):
        self._capacity = capacity

    def capacity(self):
        return self._capacity

    def find(self, nom):
        res = [ t for t in self._contents if t.has_name(nom) ]
        return res[0] if len(res)>0  else None

class Thing:
    def __init__(self,volume, name):
        self._volume = volume
        self._name = name

    def set_name(self,name):
        self._name = name

    def volume(self):
        return self._volume

    def name(self):
        return self._name

    def has_name(self, nom):
        return self.name() == nom

    def __repr__(self):
        return "Thing(%s,%s)" %( str(self.volume()), self.name() )
