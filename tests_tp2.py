from tp2 import *

def test_box_create():
    b = Box()

def test_box_contains():
    b = Box()
    b.open()
    b.add("truc")
    b.add("machin")
    assert "truc" in b
    assert "bidule" not in b
    assert "machin" in b

def test_box_remove():
    b = Box()
    b.add("truc")
    b.add("machin")
    b.remove("truc")
    assert "machin" in b
    assert "truc" not in b


def test_box_open():
    b = Box()
    # une boite doit être fermée par défaut
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_action_look():
    b = Box()
    b.add("truc")
    b.add("machin")
    assert b.action_look() == "la boite est fermée !"
    b.open()
    assert b.action_look() == "la boite contient: truc, machin"

# def test_thing_create_simple():
#     chose = Thing(3)

def test_thing_create():
    chose = Thing(3,"chose")
    assert chose.name() == "chose"
    assert chose.has_name("chose")
    chose.set_name('truc')
    assert chose.has_name("truc")
    assert chose.volume() == 3

def test_thing_repr():
    chose = Thing(5,"truc")
    assert repr(chose) == "Thing(5,truc)"

def test_box_contains_thing():
    b = Box()
    b.open()
    t1 = Thing(3,"essai")
    t2 = Thing(5,"machin")
    b.action_add(t1)
    assert t1 in b
    assert t2 not in b

def test_box_action_add_capacity_infinie():
    b = Box()
    t1 = Thing(5,"chose1")
    t2 = Thing(25,"chose2")
    # Echec car b est fermée
    assert not b.action_add(t1)
    b.open()
    assert b.action_add(t1)
    assert b.action_add(t2)

def test_box_action_add_capacity_finie():
    b = Box()
    b.set_capacity(20)
    t1 = Thing(5,"chose1")
    t2 = Thing(25,"chose2")
    b.open()
    assert b.has_room_for(t1)
    assert not b.has_room_for(t2)
    assert not b.action_add(t2)

def test_box_capacity():
    b = Box()
    t = Thing(100000,"Baleine")
    assert b.has_room_for(t)
    b.set_capacity(1000)
    assert not b.has_room_for(t)

def test_box_find():
    b = Box()
    t1 = Thing(5,"machin")
    t2 = Thing(25,"chose")
    b.open()
    b.action_add(t1)
    b.action_add(t2)
    res = b.find("chose")
    assert isinstance(res , Thing)
    assert res.has_name("chose")
    assert res.volume() == 25
    rien = b.find("tagliatelle")
    assert rien == None
